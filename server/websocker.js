const wss = require("ws").Server;
const server = new wss({ 
    port: 5777 
});

const clients = new Set();

server.on("listening", () => console.log("listen 5777"));

server.on("connection", (socket) => {
    console.log('Полкючение клиента');
    clients.add(socket);
    socket.on("message", (message) => {
        console.log('пришло сообщение');
        for (let client of clients){
            
            // if (client !== socket){
                client.send(JSON.stringify( {
                    author: client !== socket,
                    text: message
                }));
            // }
        }
    })
   socket.on("close", () => {
    clients.delete(socket)
    console.log('Клиент отключился');
   })
});
